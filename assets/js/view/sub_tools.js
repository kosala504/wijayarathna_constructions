    
    window.onload = hideErrorMessages();

    function hideErrorMessages(){
        $("#edit-error_sub").hide();
        $("#edit-error_cost").hide();
        $("#error_current_site").hide();
        $("#error_taken").hide();
        $("#error_date").hide();
        hide_loading();
    }

    $(document).ready( function () {

        //$('#dataTables-user-log').DataTable();
        $('#dataTables-user-list').DataTable({
            "bFilter": true,
            "paging":   false,
            //"iDisplayLength": 20,
            "order": [[ 0, "asc" ]]
            //"bDestroy": true,
        });
     } );

    function tool_transfer_popup(sub_tool_id,sub_tool_code,current_site,taken_by,date_taken){
        $("#sub-tool-id").val(sub_tool_id);
        $("#sub-tool-code").val(sub_tool_code);
        $("#sub-tool-site").val(current_site);
        $("#sub-tool-taken").val(taken_by);
        $("#transfer-date").val(date_taken);
        $('#toolTransferSubmit').attr("onclick","transfer_sub_tool("+sub_tool_id+","+sub_tool_code+","+current_site+","+taken_by+","+date_taken+")");

    function edit_sub_tool_popup(sub_tool_id,sub_tool_code,cost){
        $("#edit-sub-tool-id").val(sub_tool_id);
        $("#edit-sub-tool-code").val(sub_tool_code);
        $("#edit-cost").val(cost);
        $('#editSubToolSubmit').attr("onclick","update_sub_tool_details("+sub_tool_id+","+sub_tool_code+","+cost+")");
    }

    function deactivate_confirmation(sub_tool_code,sub_tool_id){
        $( "#sub-tool-del" ).html(sub_tool_code);
        $('#deactivateYesButton').attr("onclick","deactivate_submit('"+sub_tool_code+"',"+sub_tool_id+")");
    }

    function deactivate_submit(labour_name,labour_id){
        show_loading();
            $.ajax({
                url: $("#base-url").val()+"Sub_tool/delete_sub_tool/",
                traditional: true,
                type: "post",
                dataType: "text",
                data: {sub_tool_code:sub_tool_code, sub_tool_id:sub_tool_id},
                success: function (result) {
                    var result = $.parseJSON(result);
                    if(result.status=='success'){
                        location.reload();
                    }
                    else{
                        alert("Oops there is something wrong!");
                    }
                },
                error: ajax_error_handling
            });
    }

    function update_sub_tool_details(sub_tool_id){
        hideErrorMessages();
        show_loading();
        var i=0;
        var sub_tool_code = $('#edit-sub_tool-code').val().trim();
        var cost = $('#edit-sub-tool-cost').val().trim();
        if(sub_tool_code == ""){
            $("#edit-error_sub").show();
            i++;
        }

        if(cost == ""){
            $("#edit-error_cost").show();
            i++;
        }

        if(i == 0){
            $.ajax({
                url: $("#base-url").val()+"Sub_tools/update_tool_details/",
                traditional: true,
                type: "post",
                dataType: "text",
                data: {sub_tool_id:sub_tool_id,sub_tool_code:sub_tool_code,cost:cost},
                success: function (result) {
                    var result = $.parseJSON(result);
                    if(result.status=='success'){
                        location.reload();
                    }
                    else if(result.status=='exist'){
                        $("#edit-error_name").show();
                        hide_loading();
                    }
                    else{
                        alert("Oops there is something wrong!");
                    }
                },
                error: ajax_error_handling
            });
        }else{
            hide_loading();
        }
          
    }

    function transfer_sub_tool(sub_tool_id){
        alert("Transfer");
        hideErrorMessages();
        show_loading();
        var i=0;
        var sub_tool_code = $('#sub-tool-code').val().trim();
        var sub_tool_site = $('#sub-tool-site').val().trim();
        var sub_tool_taken = $('#sub-tool-taken').val().trim();
        var date_taken = $('#transfer-date').val().trim();

        if(sub_tool_code == ""){
            $("#error_sub").show();
            i++;
        }

        if(sub_tool_site == ""){
            $("#error_current_site").show();
            i++;
        }

        if(sub_tool_taken == ""){
            $("#error_taken").show();
            i++;
        }

        if(date_taken == ""){
            $("#error_date").show();
            i++;
        }

        if(i == 0){
            $.ajax({
                url: $("#base-url").val()+"Sub_tools/transfer_sub_tool/",
                traditional: true,
                type: "post",
                dataType: "text",
                data: {sub_tool_id:sub_tool_id,sub_tool_code:sub_tool_code,sub_tool_site:sub_tool_site,sub_tool_taken:sub_tool_taken,date_taken:date_taken},
                success: function (result) {
                    var result = $.parseJSON(result);
                    if(result.status=='success'){
                        location.reload();
                    }
                    else if(result.status=='exist'){
                        $("#edit-error_name").show();
                        hide_loading();
                    }
                    else{
                        alert("Oops there is something wrong!");
                    }
                },
                error: ajax_error_handling
            });
        }else{
            hide_loading();
        }
          
    }
}

    




