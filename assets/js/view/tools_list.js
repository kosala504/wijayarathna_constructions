    
    window.onload = hideErrorMessages();

    function hideErrorMessages(){
        $("#error_tool_type").hide();
        $("#error_tool_code").hide();
        $("#edit-error_tool_type").hide();
        $("#edit-error_tool_code").hide();
        $("#error_sub_code").hide();
        $("#error_sub_purch").hide();
        $("#error_sub_cost").hide();
        hide_loading();
    }

    $(document).ready( function () {

        //$('#dataTables-user-log').DataTable();
        $('#dataTables-user-list').DataTable({
            "bFilter": true,
            "paging":   false,
            //"iDisplayLength": 20,
            "order": [[ 0, "asc" ]]
            //"bDestroy": true,
        });
     } );

    function add_sub_tool_popup(tool_id,tool_type){
        $("#add-sub-id").val(tool_id);
        $("#add-sub_type").val(tool_type);
        $('#newSubToolSubmit').attr("onclick","add_subTool("+tool_id+")");
    }

    function edit_tool_popup(tool_id,tool_type,tool_code){
        $("#edit-tool-id").val(tool_id);
        $("#edit-tool_type").val(tool_type);
        $("#edit-tool_code").val(tool_code);
        $('#editToolSubmit').attr("onclick","update_tool_details("+tool_id+")");
    }

    function deactivate_confirmation(tool_type,tool_id){
        $( "#tool_del" ).html(tool_type);
        $('#deactivateYesButton').attr("onclick","deactivate_submit('"+tool_type+"',"+tool_id+")");
    }

    function deactivate_submit(tool_type,tool_id){
        show_loading();
            $.ajax({
                url: $("#base-url").val()+"Tools/delete_tool/",
                traditional: true,
                type: "post",
                dataType: "text",
                data: {tool_type:tool_type, tool_id:tool_id},
                success: function (result) {
                    var result = $.parseJSON(result);
                    if(result.status=='success'){
                        location.reload();
                    }
                    else{
                        alert("Oops there is something wrong!");
                    }
                },
                error: ajax_error_handling
            });
    }

    function update_tool_details(tool_id){
        hideErrorMessages();
        show_loading();
        var i=0;
        var tool_type = $('#edit-tool_type').val().trim();
        var tool_code = $('#edit-tool_code').val().trim();
        if(tool_type == ""){
            $("#edit-error_tool_type").show();
            i++;
        }

        if(tool_code == ""){
            $("#edit-error_tool_code").show();
            i++;
        }

        if(i == 0){
            $.ajax({
                url: $("#base-url").val()+"Tools/update_tool_details/",
                traditional: true,
                type: "post",
                dataType: "text",
                data: {tool_id: tool_id,tool_type: tool_type, tool_code:tool_code},
                success: function (result) {
                    var result = $.parseJSON(result);
                    if(result.status=='success'){
                        location.reload();
                    }
                    else if(result.status=='exist'){
                        $("#edit-error_name").show();
                        hide_loading();
                    }
                    else{
                        alert("Oops there is something wrong!");
                    }
                },
                error: ajax_error_handling
            });
        }else{
            hide_loading();
        }
          
    }


    $( "#newToolSubmit" ).click(function() {
        hideErrorMessages();
        show_loading();
        var i=0;
        var tool_type = $('#tool_type').val().trim();
        var tool_code = $('#tool_code').val().trim();
        if(tool_type == ""){
            $("#error_tool_type").show();
            i++;
        }

        if(tool_code == ""){
            $("#error_tool_code").show();
            i++;
        }

        if(i == 0){
            $.ajax({
                url: $("#base-url").val() + "tools/add_tool",
                traditional: true,
                type: "post",
                dataType: "text",
                data: {tool_type:tool_type, tool_code:tool_code},
                success: function (result) {
                    var result = $.parseJSON(result);
                    if(result.status=='success'){
                        location.reload();
                    }
                    else if(result.status=='exist'){
                        $("#error_email2").show();
                        hide_loading();
                    }
                    else{
                        alert("Oops there is something wrong!");
                    }
                  
                },
                error: ajax_error_handling
            });
        }else{
            hide_loading();
        }
            
    });

    function add_subTool(tool_id){
            hideErrorMessages();
            show_loading();
            var i=0;
            var sub_tool_code= $('#sub_tool_code').val().trim();
            var tool_purch_date = $('#tool_purch_date').val().trim();
            var tool_cost = $('#tool_cost').val().trim();
            if(sub_tool_code == ""){
                $("#error_sub_code").show();
                i++;
            }

            if(tool_purch_date == ""){
                $("#error_sub_purch").show();
                i++;
            }

             if(tool_cost == ""){
                $("#error_sub_cost").show();
                i++;
            }

            if(i == 0){
                $.ajax({
                    url: $("#base-url").val()+"Tools/add_sub_tool/",
                    traditional: true,
                    type: "post",
                    dataType: "text",
                    data: {tool_id: tool_id,sub_tool_code: sub_tool_code, tool_purch_date:tool_purch_date,tool_cost:tool_cost},
                    success: function (result) {
                        var result = $.parseJSON(result);
                        if(result.status=='success'){
                            location.reload();
                        }
                        else if(result.status=='exist'){
                            $("#edit-error_name").show();
                            hide_loading();
                        }
                        else{
                            alert("Oops there is something wrong!");
                        }
                    },
                    error: ajax_error_handling
                });
            }else{
                hide_loading();
            }
              
        }

