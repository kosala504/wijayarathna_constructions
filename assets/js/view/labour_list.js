    
    window.onload = hideErrorMessages();

    function hideErrorMessages(){
        $("#error_name").hide();
        $("#error_nic").hide();
        $("#error_con").hide();
        $("#error_con2").hide();
        $("#error_con3").hide();
        $("#error_site").hide();
        $("#edit-error_name").hide();
        $("#edit-error_nic").hide();
        $("#edit-error_con").hide();
        $("#edit-error_con2").hide();
        $("#edit-error_con3").hide();
        $("#edit-error_site").hide();
        hide_loading();
    }

    $(document).ready( function () {

        //$('#dataTables-user-log').DataTable();
        $('#dataTables-user-list').DataTable({
            "bFilter": true,
            "paging":   false,
            //"iDisplayLength": 20,
            "order": [[ 0, "asc" ]]
            //"bDestroy": true,
        });
     } );

    function edit_labour_popup(labour_id,labour_name,nic,contact_number,current_site){
        $("#edit-labour-id").val(labour_id);
        $("#edit-labour-name").val(labour_name);
        $("#edit-nic").val(nic);
        $("#edit-contact_number").val(contact_number);
        $("#edit-current_site").val(current_site);
        $('#editLabourSubmit').attr("onclick","update_labour_details("+labour_id+")");
    }

    function deactivate_confirmation(labour_name,labour_id){
        $( "#labour_del" ).html(labour_name);
        $('#deactivateYesButton').attr("onclick","deactivate_submit('"+labour_name+"',"+labour_id+")");
    }

    function deactivate_submit(labour_name,labour_id){
        show_loading();
            $.ajax({
                url: $("#base-url").val()+"Labour/delete_labour/",
                traditional: true,
                type: "post",
                dataType: "text",
                data: {labour_name: labour_name, labour_id:labour_id},
                success: function (result) {
                    var result = $.parseJSON(result);
                    if(result.status=='success'){
                        location.reload();
                    }
                    else{
                        alert("Oops there is something wrong!");
                    }
                },
                error: ajax_error_handling
            });
    }

    function update_labour_details(labour_id){
        hideErrorMessages();
        show_loading();
        var i=0;
        var labour_name = $('#edit-labour-name').val().trim();
        var nic = $('#edit-nic').val().trim();
        var contact_number = $('#edit-contact_number').val().trim();
        var current_site = $('#edit-current_site').val();
        if(labour_name == ""){
            $("#edit-error_name").show();
            i++;
        }

        if(nic == ""){
            $("#edit-error_nic").show();
            i++;
        }

        if(contact_number == ""){
            $("#edit-error_con").show();
            i++;
        }

        else if(!contact_number.match(/^[0-9]+$/)){
            $("#edit-error_con2").show();
            i++;
        }
        else if((contact_number.toString().length)!=10){
            $("#edit-error_con3").show();
            i++;
        }

        if(current_site == 0){
            $("#edit-error_site").show();
            i++;
        }

        if(i == 0){
            $.ajax({
                url: $("#base-url").val()+"Labour/update_labour_details/",
                traditional: true,
                type: "post",
                dataType: "text",
                data: {labour_id: labour_id,labour_name: labour_name, nic:nic, contact_number:contact_number,current_site:current_site},
                success: function (result) {
                    var result = $.parseJSON(result);
                    if(result.status=='success'){
                        location.reload();
                    }
                    else if(result.status=='exist'){
                        $("#edit-error_name").show();
                        hide_loading();
                    }
                    else{
                        alert("Oops there is something wrong!");
                    }
                },
                error: ajax_error_handling
            });
        }else{
            hide_loading();
        }
          
    }


    $( "#newLabourSubmit" ).click(function() {
        hideErrorMessages();
        show_loading();
        var i=0;
        var labour_name = $('#labour_name').val().trim();
        var nic = $('#nic').val().trim();
        var contact_number = $('#contact_number').val().trim();
        var current_site = $('#current_site').val();
        if(labour_name == ""){
            $("#error_name").show();
            i++;
        }

        if(nic == ""){
            $("#error_nic").show();
            i++;
        }

        if(contact_number == ""){
            $("#error_con").show();
            i++;
        }

        else if(!contact_number.match(/^[0-9]+$/)){
            $("#error_con2").show();
            i++;
        }
        else if((contact_number.toString().length)!=10){
            $("#error_con3").show();
            i++;
        }

        if(current_site == 0){
            $("#error_site").show();
            i++;
        }

        if(i == 0){
            $.ajax({
                url: $("#base-url").val() + "labour/add_labour",
                traditional: true,
                type: "post",
                dataType: "text",
                data: {labour_name:labour_name, nic:nic,contact_number:contact_number,current_site:current_site},
                success: function (result) {
                    var result = $.parseJSON(result);
                    if(result.status=='success'){
                        location.reload();
                    }
                    else if(result.status=='exist'){
                        $("#error_email2").show();
                        hide_loading();
                    }
                    else{
                        alert("Oops there is something wrong!");
                    }
                  
                },
                error: ajax_error_handling
            });
        }else{
            hide_loading();
        }
            
    });


