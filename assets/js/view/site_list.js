    
    window.onload = hideErrorMessages();

    function hideErrorMessages(){
        $("#error_name").hide();
        $("#error_name2").hide();
        $("#edit-error_name").hide();
        $("#edit-error_name2").hide();
        hide_loading();
    }

    $(document).ready( function () {

        //$('#dataTables-user-log').DataTable();
        $('#dataTables-user-list').DataTable({
            "bFilter": true,
            "paging":   false,
            //"iDisplayLength": 20,
            "order": [[ 0, "asc" ]]
            //"bDestroy": true,
        });
     } );

    function edit_site_popup(site_name,site_id,place){
        $( "#edit-site-name" ).val(site_name);
        $( "#edit-site-id" ).val(site_id);
        $( "#edit-place" ).val(place);
        $('#editSiteSubmit').attr("onclick","update_site_details("+site_id+")");
    }

    function deactivate_confirmation(site_name,site_id){
        $( "#site_del" ).html(site_name);
        $('#deactivateYesButton').attr("onclick","deactivate_submit('"+site_name+"',"+site_id+")");
    }

    function deactivate_submit(site_name,site_id){
        show_loading();
            $.ajax({
                url: $("#base-url").val()+"Sites/delete_site/",
                traditional: true,
                type: "post",
                dataType: "text",
                data: {site_name: site_name, site_id:site_id},
                success: function (result) {
                    var result = $.parseJSON(result);
                    if(result.status=='success'){
                        location.reload();
                    }
                    else{
                        alert("Oops there is something wrong!");
                    }
                },
                error: ajax_error_handling
            });
    }

    function update_site_details(site_id){
        hideErrorMessages();
        show_loading();
        var i=0;
        var site_name = $("#edit-site-name").val().trim();
        var place = $("#edit-place").val().trim();
        if(site_name == ""){
            $("#edit-error_name").show();
            i++;
        }
        if(place == ""){
            $("#edit-error_name2").show();
            i++;
        }

        if(i == 0){
            $.ajax({
                url: $("#base-url").val()+"Sites/update_site_details/",
                traditional: true,
                type: "post",
                dataType: "text",
                data: {site_name: site_name, id:site_id, place:place},
                success: function (result) {
                    var result = $.parseJSON(result);
                    if(result.status=='success'){
                        location.reload();
                    }
                    else if(result.status=='exist'){
                        $("#edit-error_name").show();
                        hide_loading();
                    }
                    else{
                        alert("Oops there is something wrong!");
                    }
                },
                error: ajax_error_handling
            });
        }else{
            hide_loading();
        }
          
    }


    $( "#newSiteSubmit" ).click(function() {
        hideErrorMessages();
        show_loading();
        var i=0;
        var site_name = $('#site_name').val().trim();
        var place = $('#place').val().trim();
        if(site_name == ""){
            $("#error_name").show();
            i++;
        }

        if(place == ""){
            $("#error_name2").show();
            i++;
        }

        if(i == 0){
            $.ajax({
                url: $("#base-url").val() + "sites/add_site",
                traditional: true,
                type: "post",
                dataType: "text",
                data: {site_name:site_name, place:place},
                success: function (result) {
                    var result = $.parseJSON(result);
                    if(result.status=='success'){
                        location.reload();
                    }
                    else if(result.status=='exist'){
                        $("#error_email2").show();
                        hide_loading();
                    }
                    else{
                        alert("Oops there is something wrong!");
                    }
                  
                },
                error: ajax_error_handling
            });
        }else{
            hide_loading();
        }
            
    });


