<?php

    /*************************************************
    *       Developer  :  kosala504@gmail.com        *
    *        Copyright © 2022 Kosala Hasantha        *
    **************************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Tools_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }


    function get_tools_list(){
        $this->db->select('*');
        $this->db->from('tools');
        $query=$this->db->get();
        return $query->result();
    }

    function get_tool_by_id($toolID){
        $this->db->select('*');
        $this->db->from('tools');
        $this->db->where('tool_id', $toolID);
        $query=$this->db->get();
        return $query->result_array();
    }

    function get_tool_count(){
        $this->db->select('*');
        $this->db->from('sub_tools');
        $this->db->where('main_tool_id',"1");
        $query = $this->db->count_all_results();
        return $query;
    }

    function insert_tool($postData){
        $data = array(
            'tool_type' => $postData['tool_type'],
            'tool_code' => $postData['tool_code']
        );
        $this->db->insert('tools', $data);

        $module = "Tools Management";
        $activity = "add new tool ".$postData['tool_type'];
        $this->insert_log($activity, $module);
        return array('status' => 'success', 'message' => '');

        
    }

    function update_tool_details($postData){

        $oldData = $this->get_tool_by_id($postData['tool_id']);

            $data = array(
                'tool_type' => $postData['tool_type'],
                'tool_code' => $postData['tool_code'],
            );
            $this->db->where('tool_id', $postData['tool_id']);
            $this->db->update('tools', $data);

            $record = "(".$oldData[0]['tool_type']." to ".$postData['tool_type'].",".$oldData[0]['tool_code']." to ".$postData['tool_code'].")";

            $module = "Stores Management";
            $activity = "update tool ".$oldData[0]['tool_type']."`s details ".$record;
            $this->insert_log($activity, $module);
            return array('status' => 'success', 'message' => $record);
        

    }


    function delete_tool($postData){
        $data = array(
                'tool_type' => $postData['tool_type'],
                'tool_id' => $postData['tool_id'],
            );
        $this->db->where('tool_id', $postData['tool_id']);
        $this->db->delete('tools');

        $module = "Stores";
        $activity = "delete tool ".$postData['tool_type'];
        $this->insert_log($activity, $module);
        return array('status' => 'success', 'message' => '');

    }

    function insert_sub_tool($postData){
        $data = array(
            'main_tool_id' => $postData['tool_id'],
            'sub_tool_code' => $postData['sub_tool_code'],
            'purch_date' => $postData['tool_purch_date'],
            'cost' => $postData['tool_cost']
        );
        $this->db->insert('sub_tools', $data);

        $module = "Sub Tool Management";
        $activity = "add new sub tool ".$postData['sub_tool_code'];
        $this->insert_log($activity, $module);
        return array('status' => 'success', 'message' => '');

        
    }

    function insert_log($activity, $module){
        $id = $this->session->userdata('user_id');

        $data = array(
            'fk_user_id' => $id,
            'activity' => $activity,
            'module' => $module,
            'created_at' => date('Y\-m\-d\ H:i:s A')
        );
        $this->db->insert('activity_log', $data);
    }

    function send_email($message,$subject,$sendTo){
        require_once APPPATH.'libraries/mailer/class.phpmailer.php';
        require_once APPPATH.'libraries/mailer/class.smtp.php';
        require_once APPPATH.'libraries/mailer/mailer_config.php';
        include APPPATH.'libraries/mailer/template/template.php';
        
        $mail = new PHPMailer(true);
        $mail->IsSMTP();
        try
        {
            $mail->SMTPDebug = 1;  
            $mail->SMTPAuth = true; 
            $mail->SMTPSecure = 'ssl'; 
            $mail->Host = HOST;
            $mail->Port = PORT;  
            $mail->Username = GUSER;  
            $mail->Password = GPWD;     
            $mail->SetFrom(GUSER, 'Administrator');
            $mail->Subject = "DO NOT REPLY - ".$subject;
            $mail->IsHTML(true);   
            $mail->WordWrap = 0;


            $hello = '<h1 style="color:#333;font-family:Helvetica,Arial,sans-serif;font-weight:300;padding:0;margin:10px 0 25px;text-align:center;line-height:1;word-break:normal;font-size:38px;letter-spacing:-1px">Hello, &#9786;</h1>';
            $thanks = "<br><br><i>This is autogenerated email please do not reply.</i><br/><br/>Thanks,<br/>Admin<br/><br/>";

            $body = $hello.$message.$thanks;
            $mail->Body = $header.$body.$footer;
            $mail->AddAddress($sendTo);

            if(!$mail->Send()) {
                $error = 'Mail error: '.$mail->ErrorInfo;
                return array('status' => false, 'message' => $error);
            } else { 
                return array('status' => true, 'message' => '');
            }
        }
        catch (phpmailerException $e)
        {
            $error = 'Mail error: '.$e->errorMessage();
            return array('status' => false, 'message' => $error);
        }
        catch (Exception $e)
        {
            $error = 'Mail error: '.$e->getMessage();
            return array('status' => false, 'message' => $error);
        }
        
    }

}

/* End of file */