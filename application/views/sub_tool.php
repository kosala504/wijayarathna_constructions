

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><?=$title?></h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <?php if($this->session->flashdata('success')):?>
                <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong><?php echo $this->session->flashdata('success'); ?></strong>
                </div>
            <?php elseif($this->session->flashdata('error')):?>       
                <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong><?php echo $this->session->flashdata('error'); ?></strong>
                </div>
            <?php endif;?>
            <div class="row">
                <div class="col-lg-12">      
                    <table class="table table-striped table-bordered table-hover" id="dataTables-user-list">
                        <thead>
                            <tr>
                                <th>Tool Code</th>
                                <th>Cost</th>
                                <th>Current Site</th>
                                <th>Taken By</th>
                                <th>Transfer Date</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($sub_tools as $row): ?>
                            <tr>
                                <td><?php echo $row->sub_tool_code; ?></td> 
                                <td><?php echo $row->cost; ?></td>
                                <td><?php echo $row->current_site; ?></td>
                                <td><?php echo $row->taken_by; ?></td>
                                <td><?php echo $row->date_taken; ?></td>
                                <td>
                                	<a class="btn btn-success" id="tool_transfer"  onclick="tool_transfer_popup('<?=$row->sub_tool_id?>','<?=$row->sub_tool_code?>','<?=$row->current_site?>','<?=$row->taken_by?>','<?=$row->date_taken?>');" data-toggle="modal" data-target="#toolTransfer"> Transfer </a>
                                    <a class="btn btn-primary" id="sub-tool-edit"  onclick="edit_sub_tool_popup('<?=$row->sub_tool_id?>','<?=$row->sub_tool_code?>','<?=$row->cost?>');" data-toggle="modal" data-target="#editSubTool"> EDIT </a>
                                    <a class="btn btn-danger" id="sub-tool-delete" onclick="deactivate_confirmation('<?=$row->sub_tool_code?>','<?=$row->sub_tool_id?>');" data-toggle="modal" data-target="#deactivateConfirm"> DELETE </a>
                                    
                                </td>

                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                   </div>

                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>

        <!-- Modal -->
        <div class="modal fade" id="deactivateConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-red">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">DELETE CONFIRMATION</h4>
                    </div>
                    <div class="modal-body">
                        <label>You are going to delete tool <label id="sub-tool-del" style="color:blue;"></label>?</label><br/>
                        <label>Click <b>Yes</b> to continue.</label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <a id="deactivateYesButton" class="btn btn-danger" >Yes</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="editSubTool" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-blue">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">UPDATE SUB TOOL DETAILS</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden"  id="edit-sub-tool-id" value=""/>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Tool code</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_sub"> field is required.</label>
                                    <input class="form-control" id="edit-sub-tool-code" placeholder="Tool Code" name="edit-sub-tool-code" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Cost</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_cost"> field is required.</label>
                                    <input class="form-control" id="edit-cost" placeholder="Cost" name="edit-cost" type="text" autofocus>
                                </div> 
                            </div>
                      </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                        <button id="editSubToolSubmit" type="button" class="btn btn-primary">UPDATE</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="toolTransfer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-blue">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">TRANSFER TOOL</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden"  id="sub-tool-id" value=""/>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Tool code</label> &nbsp;&nbsp;
                                    <input class="form-control" id="sub-tool-code" placeholder="Tool Code" name="sub-tool-code" type="text" autofocus readonly>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Current site</label> &nbsp;&nbsp;
                                    <label class="error" id="error_current_site"> field is required.</label>
                                    <input class="form-control" id="sub-tool-site" placeholder="Site" name="sub-tool-site" type="text" autofocus>
                                </div> 
                            </div>
                        </div>
                            <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Taken by</label> &nbsp;&nbsp;
                                    <label class="error" id="error_taken"> field is required.</label>
                                    <input class="form-control" id="sub-tool-taken" placeholder="Taken by" name="sub-tool-taken" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Transfer Date</label> &nbsp;&nbsp;
                                    <label class="error" id="error_date"> field is required.</label>
                                    <input class="form-control" id="transfer-date" placeholder="Trasfer Date" name="transfer-date" type="text" autofocus>
                                </div> 
                            </div>
                      </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                        <button id="toolTransferSubmit" type="button" class="btn btn-primary">Transfer</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
       
        <!-- /#page-wrapper -->
        <?php $this->load->view('frame/footer_view')?>
        <script src="<?=base_url()?>assets/js/view/sub_tools.js"></script>