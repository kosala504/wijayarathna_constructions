

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><?=$title?></h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <?php if($this->session->flashdata('success')):?>
                <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong><?php echo $this->session->flashdata('success'); ?></strong>
                </div>
            <?php elseif($this->session->flashdata('error')):?>       
                <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong><?php echo $this->session->flashdata('error'); ?></strong>
                </div>
            <?php endif;?>
            <div class="row">
                <div class="col-lg-12">      
                    <table class="table table-striped table-bordered table-hover" id="dataTables-user-list">
                        <thead>
                            <tr>
                                <th>Labour ID</th>
                                <th>Labour Name</th>
                                <th>NIC</th>
                                <th>Contact Number</th>
                                <th>Current Site</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($labour as $row): ?>
                            <tr>
                                <td><?php echo $row->labour_id; ?></td> 
                                <td><?php echo $row->labour_name; ?></td>
                                <td><?php echo $row->nic; ?></td>
                                <td><?php echo $row->contact_number; ?></td>
                                <td><?php echo $row->current_site; ?></td>
                                <td>
                                    <a class="btn btn-primary" id="labour-edit"  onclick="edit_labour_popup('<?=$row->labour_id?>','<?=$row->labour_name?>','<?=$row->nic?>','<?=$row->contact_number?>','<?=$row->current_site?>');" data-toggle="modal" data-target="#editLabour"> EDIT </a>
                                    <a class="btn btn-danger" id="labour-delete" onclick="deactivate_confirmation('<?=$row->labour_name?>','<?=$row->labour_id?>');" data-toggle="modal" data-target="#deactivateConfirm"> DELETE </a>
                                    
                                </td>

                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                   </div>
                    <div class="col-lg-12" style="position:fixed;bottom: 5%;left: 88%; width: 150px;text-align: center;border-radius: 100%;">
                        <img class="add_user" src="<?=base_url()?>assets/images/add.png" data-toggle="modal" data-target="#addLabour" />
                    </div>

                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>

        <!-- Modal -->
        <div class="modal fade" id="deactivateConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-red">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">DELETE CONFIRMATION</h4>
                    </div>
                    <div class="modal-body">
                        <label>You are going to delete labour <label id="labour_del" style="color:blue;"></label>?</label><br/>
                        <label>Click <b>Yes</b> to continue.</label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <a id="deactivateYesButton" class="btn btn-danger" >Yes</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


        <div class="modal fade" id="addLabour" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-blue">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">ADD NEW LABOUR</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Labour Name</label> &nbsp;&nbsp;
                                    <label class="error" id="error_name"> field is required.</label>
                                    <input class="form-control" id="labour_name" placeholder="Labour Name" name="labour_name" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>NIC</label> &nbsp;&nbsp;
                                    <label class="error" id="error_nic"> field is required.</label>
                                    <input class="form-control" id="nic" placeholder="NIC" name="nic" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Contact Number</label> &nbsp;&nbsp;
                                    <label class="error" id="error_con"> field is required.</label>
                                    <label class="error" id="error_con2"> *numbers only.</label>
                                    <label class="error" id="error_con3"> invalid number.</label>
                                    <input class="form-control" id="contact_number" placeholder="Contact Number" name="contact_number" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Current Site</label> &nbsp;&nbsp;
                                    <label class="error" id="error_site"> please select site.</label>
                                    <select name="current_site" id="current_site" class="form-control" >
                                        <option value="0" selected="selected">-- SELECT SITE --</option>
                                    <?php foreach($sites as $row): ?>
                                        <option value="<?php echo $row->site_name; ?>"><?php echo $row->site_name; ?></option>
                                    <?php endforeach; ?>    
                                    </select> 
                                </div> 
                            </div>
                      </div>
                      
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                        <button id="newLabourSubmit" type="button" class="btn btn-primary">CREATE</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


        <div class="modal fade" id="editLabour" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-blue">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">UPDATE LABOUR DETAILS</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden"  id="edit-labour-id" value=""/>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Labour Name</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_name"> field is required.</label>
                                    <input class="form-control" id="edit-labour-name" placeholder="Labour Name" name="edit-labour-name" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>NIC</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_nic"> field is required.</label>
                                    <input class="form-control" id="edit-nic" placeholder="NIC" name="edit-nic" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Contact Number</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_con"> field is required.</label>
                                    <label class="error" id="edit-error_con2"> *numbers only.</label>
                                    <label class="error" id="edit-error_con3"> invalid number.</label>
                                    <input class="form-control" id="edit-contact_number" placeholder="Contact Number" name="edit-contact_number" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Current Site</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_site"> please select site.</label>
                                    <select name="edit-current_site" id="edit-current_site" class="form-control" >
                                        <option value="0" selected="selected">-- SELECT SITE --</option>
                                    <?php foreach($sites as $row): ?>
                                        <option value="<?php echo $row->site_name; ?>"><?php echo $row->site_name; ?></option>
                                    <?php endforeach; ?>    
                                    </select> 
                                </div> 
                            </div>
                      </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                        <button id="editLabourSubmit" type="button" class="btn btn-primary">UPDATE</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
       
        <!-- /#page-wrapper -->
        <?php $this->load->view('frame/footer_view')?>
        <script src="<?=base_url()?>assets/js/view/labour_list.js"></script>