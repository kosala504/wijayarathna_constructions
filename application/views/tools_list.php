

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><?=$title?></h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <?php if($this->session->flashdata('success')):?>
                <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong><?php echo $this->session->flashdata('success'); ?></strong>
                </div>
            <?php elseif($this->session->flashdata('error')):?>       
                <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong><?php echo $this->session->flashdata('error'); ?></strong>
                </div>
            <?php endif;?>
            <div class="row">
                <div class="col-lg-12">      
                    <table class="table table-striped table-bordered table-hover" id="dataTables-user-list">
                        <thead>
                            <tr>
                                <th>Tool Type</th>
                                <th>Tool Code</th>
                                <th>Tool Count</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($tools as $row): ?>
                            <tr>
                                <td><?php echo $row->tool_type; ?></td> 
                                <td><?php echo $row->tool_code; ?></td>
                                <td><?php echo $tool_count; ?></td>
                                <td>
                                    <a class="btn btn-success" id="tool-edit"  onclick="add_sub_tool_popup('<?=$row->tool_id?>','<?=$row->tool_type?>');" data-toggle="modal" data-target="#addSubTool"> ADD SUB TOOL </a>
                                    <a class="btn btn-primary" id="tool-edit"  onclick="edit_tool_popup('<?=$row->tool_id?>','<?=$row->tool_type?>','<?=$row->tool_code?>');" data-toggle="modal" data-target="#editTool"> EDIT </a>
                                    <a class="btn btn-danger" id="tool-delete" onclick="deactivate_confirmation('<?=$row->tool_type?>','<?=$row->tool_id?>');" data-toggle="modal" data-target="#deactivateConfirm"> DELETE </a>
                                    
                                </td>

                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                   </div>
                    <div class="col-lg-12" style="position:fixed;bottom: 5%;left: 88%; width: 150px;text-align: center;border-radius: 100%;">
                        <img class="add_user" src="<?=base_url()?>assets/images/add.png" data-toggle="modal" data-target="#addTool" />
                    </div>

                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>

        <!-- Modal -->
        <div class="modal fade" id="deactivateConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-red">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">DELETE CONFIRMATION</h4>
                    </div>
                    <div class="modal-body">
                        <label>You are going to delete tool <label id="tool_del" style="color:blue;"></label>?</label><br/>
                        <label>Click <b>Yes</b> to continue.</label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <a id="deactivateYesButton" class="btn btn-danger" >Yes</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


        <div class="modal fade" id="addTool" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-blue">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">ADD NEW TOOL</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Tool Type</label> &nbsp;&nbsp;
                                    <label class="error" id="error_tool_type"> field is required.</label>
                                    <input class="form-control" id="tool_type" placeholder="Tool Type" name="tool_type" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Tool Code</label> &nbsp;&nbsp;
                                    <label class="error" id="error_tool_code"> field is required.</label>
                                    <input class="form-control" id="tool_code" placeholder="Tool Code" name="tool_code" type="text" autofocus>
                                </div> 
                            </div>
                      </div>
                      
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                        <button id="newToolSubmit" type="button" class="btn btn-primary">CREATE</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


        <div class="modal fade" id="editTool" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-blue">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">UPDATE TOOL DETAILS</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden"  id="edit-tool-id" value=""/>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Tool Type</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_tool_type"> field is required.</label>
                                    <input class="form-control" id="edit-tool_type" placeholder="Tool Type" name="edit-tool_type" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Tool Code</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_tool_code"> field is required.</label>
                                    <input class="form-control" id="edit-tool_code" placeholder="Tool Code" name="edit-tool_code" type="text" autofocus>
                                </div> 
                            </div>
                      </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                        <button id="editToolSubmit" type="button" class="btn btn-primary">UPDATE</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <div class="modal fade" id="addSubTool" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-blue">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">ADD SUB TOOL</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Sub Tool Code</label> &nbsp;&nbsp;
                                    <label class="error" id="error_sub_code"> field is required.</label>
                                    <input class="form-control" id="sub_tool_code" placeholder="Sub tool code" name="sub_tool_code" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Date of purchase</label> &nbsp;&nbsp;
                                    <label class="error" id="error_sub_purch"> field is required.</label>
                                    <input type="text" class="form-control" id="tool_purch_date" placeholder="Tool purchase date" name="tool_purch_date">
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Cost</label> &nbsp;&nbsp;
                                    <label class="error" id="error_sub_cost"> field is required.</label>
                                    <input class="form-control" id="tool_cost" placeholder="Tool cost" name="tool_cost" type="text" autofocus>
                                </div> 
                            </div>
                      </div>
                      
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                        <button id="newSubToolSubmit" type="button" class="btn btn-primary">ADD</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- /#page-wrapper -->
        <?php $this->load->view('frame/footer_view')?>
        <script src="<?=base_url()?>assets/js/view/tools_list.js"></script>