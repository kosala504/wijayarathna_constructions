<?php

    /*************************************************
    *       Developer  :  kosala504@gmail.com        *
    *        Copyright © 2022 Kosala Hasantha        *
    **************************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Tools extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }
        $this->load->model('Tools_model');

    }

    public function index() {
        $data = array(
            'formTitle' => 'Stores Management',
            'title' => 'Stores Management',
            'tools' => $this->Tools_model->get_tools_list(),
            'tool_count' => $this->Tools_model->get_tool_count(),
        );

        $this->load->view('frame/header_view');
        $this->load->view('frame/sidebar_nav_view');
        $this->load->view('tools_list',$data);
    }
    
     private function ajax_checking(){
        if (!$this->input->is_ajax_request()) {
            redirect(base_url());
        }
    }
   

    public function add_tool(){
        $this->ajax_checking();
        
        $postData = $this->input->post();
        $insert = $this->Tools_model->insert_tool($postData);
        if($insert['status'] == 'success')
            $this->session->set_flashdata('success', 'Tool '.$postData['tool_type'].' has been successfully created!');

        echo json_encode($insert);
    }

    function update_tool_details(){
        $this->ajax_checking();

        $postData = $this->input->post();
        $update = $this->Tools_model->update_tool_details($postData);
        if($update['status'] == 'success')
            $this->session->set_flashdata('success', 'Tool '.$postData['tool_type'].'`s details have been successfully updated!');

        echo json_encode($update);
    }

    function delete_tool(){  
        $this->ajax_checking();

        $postData = $this->input->post();
        $delete = $this->Tools_model->delete_tool($postData);
        if($delete['status'] == 'success')
            $this->session->set_flashdata('success', 'Tool '.$postData['tool_type'].' has been successfully deleted!');

        echo json_encode($delete);
    }

    public function add_sub_tool(){
        $this->ajax_checking();
        
        $postData = $this->input->post();
        $insert = $this->Tools_model->insert_sub_tool($postData);
        if($insert['status'] == 'success')
            $this->session->set_flashdata('success', 'Sub Tool '.$postData['sub_tool_code'].' has been successfully created!');

        echo json_encode($insert);
    }

}

/* End of file */
