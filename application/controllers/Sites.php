<?php

    /*************************************************
    *       Developer  :  kosala504@gmail.com        *
    *        Copyright © 2022 Kosala Hasantha        *
    **************************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Sites extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }
        $this->load->model('Sites_model');

    }

    public function index() {
        $data = array(
            'formTitle' => 'Site Management',
            'title' => 'Site Management',
            'sites' => $this->Sites_model->get_sites_list(),
        );

        $this->load->view('frame/header_view');
        $this->load->view('frame/sidebar_nav_view');
        $this->load->view('sites_list',$data);
    }
    
     private function ajax_checking(){
        if (!$this->input->is_ajax_request()) {
            redirect(base_url());
        }
    }
   

    public function add_site(){
        $this->ajax_checking();
        
        $postData = $this->input->post();
        $insert = $this->Sites_model->insert_site($postData);
        if($insert['status'] == 'success')
            $this->session->set_flashdata('success', 'Site '.$postData['site_name'].' has been successfully created!');

        echo json_encode($insert);
    }

    function update_site_details(){
        $this->ajax_checking();

        $postData = $this->input->post();
        $update = $this->Sites_model->update_site_details($postData);
        if($update['status'] == 'success')
            $this->session->set_flashdata('success', 'Site '.$postData['site_name'].'`s details have been successfully updated!');

        echo json_encode($update);
    }

    function delete_site(){  
        $this->ajax_checking();

        $postData = $this->input->post();
        $delete = $this->Sites_model->delete_site($postData);
        if($delete['status'] == 'success')
            $this->session->set_flashdata('success', 'Site '.$postData['site_name'].' has been successfully deleted!');

        echo json_encode($delete);
    }

}

/* End of file */
