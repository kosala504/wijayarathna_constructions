<?php

    /*************************************************
    *       Developer  :  kosala504@gmail.com        *
    *        Copyright © 2022 Kosala Hasantha        *
    **************************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Sub_tools extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }
        $this->load->model('Sub_tool_model');

    }

    public function index() {
        $data = array(
            'formTitle' => 'Sub Tool Management',
            'title' => 'Sub Tool Management',
            'sub_tools' => $this->Sub_tool_model->get_sub_tool_list()
        );

        $this->load->view('frame/header_view');
        $this->load->view('frame/sidebar_nav_view');
        $this->load->view('sub_tool',$data);
    }
    
     private function ajax_checking(){
        if (!$this->input->is_ajax_request()) {
            redirect(base_url());
        }
    }

    function update_sub_tool_details(){
        $this->ajax_checking();

        $postData = $this->input->post();
        $update = $this->Labour_model->update_labour_details($postData);
        if($update['status'] == 'success')
            $this->session->set_flashdata('success', 'Labour '.$postData['labour_name'].'`s details have been successfully updated!');

        echo json_encode($update);
    }

    function delete_sub_tool(){  
        $this->ajax_checking();

        $postData = $this->input->post();
        $delete = $this->Sub_tool_model->delete_sub_tool($postData);
        if($delete['status'] == 'success')
            $this->session->set_flashdata('success', 'Sub tool '.$postData['sub_tool_code'].' has been successfully deleted!');

        echo json_encode($delete);
    }

}

/* End of file */
